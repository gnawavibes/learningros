#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

class Node1(Node):
    def __init__(self):
        super().__init__("first_node")    # node name
        self.get_logger().info("Hello from a node made of a Node1 type. this is the way we do logs in ROS2")
        self.counter_ = 0
        self.get_logger().info("Let's count!")
        self.create_timer(1.0,self.timer_callback)

    def timer_callback(self):
        self.get_logger().info("count: " + str(self.counter_))
        self.counter_ += 1

def main(args=None):
    rclpy.init(args=args)   # Mandatory to initiate a ROS2 python program
    node = Node1()          # Create a Node
    rclpy.spin(node)        # keep the node alive
    rclpy.shutdown()        # Mandatory to end a ROS2 python program

if __name__=="__main__":
    main()
