#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from random import choice

LIST_OF_VALUES=[
    "cake",
    "ice cream",
    "fench fries",
    "pizza",
    "carbonara",
    "gros"
]


class DrawCircleNode(Node):
    """Node used to draw a circle with turtlesim"""
    def __init__(self):
        super().__init__("draw_circle")    # node name
        from rcl_interfaces.msg import ParameterDescriptor
        descr = ParameterDescriptor(description='A simple test on how to add a parameter')
        self.declare_parameter('test_param', "cake", descr)
        self.cmd_vel_pub_ = self.create_publisher(Twist, "/turtle1/cmd_vel",10) # publisher with a type and a name
        self.timer = self.create_timer(0.5,self.send_velocity_cmd)
        self.get_logger().info("draw_circle node is now instanciated")

    def send_velocity_cmd(self):
        """
        Callback function
        """
        #### Update the velocity example
        self.get_logger().info("update velocity!")
        """
        ~~~ From a terminal ~~~
        $ ros2 interface show geometry_msgs/msg/Twist
        # This expresses velocity in free space broken into its linear and angular parts.

        Vector3  linear
            float64 x
            float64 y
            float64 z
        Vector3  angular
            float64 x
            float64 y
            float64 z
        """
        msg = Twist()       # create the message
        msg.linear.x = 5.0
        msg.angular.z = 2.0
        self.cmd_vel_pub_.publish(msg)

        ####  Change parameter example
        # get param value
        param = self.get_parameter('test_param').get_parameter_value().string_value
        self.get_logger().info("The test_param currently has the value: "+str(param))
        # change param value
        my_new_param = rclpy.parameter.Parameter(
            'test_param',
            rclpy.Parameter.Type.STRING,
            choice(LIST_OF_VALUES)
        )
        all_new_parameters = [my_new_param]
        self.set_parameters(all_new_parameters)



def main(args=None):
    rclpy.init(args=args)   # Mandatory to initiate a ROS2 python program
    drawCricle = DrawCircleNode()          # Create a Node
    rclpy.spin(drawCricle)        # keep the node alive
    rclpy.shutdown()        # Mandatory to end a ROS2 python program

if __name__=="__main__":
    main()