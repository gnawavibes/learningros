#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from turtlesim.msg import Pose

class TurtlePoseSuscriber(Node):
    """Read and display turtlesim position"""
    def __init__(self):
        super().__init__("turtle_pose_subscriber")    # node name
        self.pose_subscriber = self.create_subscription(
            Pose,
            "/turtle1/pose", 
            self.pose_callback,
            10
        )
        self.get_logger().info("Read and display turtlesim position node has just been instanciated")

    def pose_callback(self, msg:Pose):
        self.get_logger().info("x=" + str(round(msg.x,4)) + "; y=" + str(round(msg.y,4)) + ";") 


def main(args=None):
    rclpy.init(args=args)   # Mandatory to initiate a ROS2 python program
    node = TurtlePoseSuscriber()          # Create a Node
    rclpy.spin(node)        # keep the node alive
    rclpy.shutdown()        # Mandatory to end a ROS2 python program

if __name__=="__main__":
    main()