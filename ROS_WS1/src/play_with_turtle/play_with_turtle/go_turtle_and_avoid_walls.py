#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from math import pi
from turtlesim.msg import Pose
from turtlesim.srv import SetPen
from geometry_msgs.msg import Twist
from functools import partial

class GoAndAvoidWalls(Node):
    """Node used to draw a circle with turtlesim"""
    def __init__(self):
        super().__init__("go_turtle_and_avoid_walls")    # node name
        self.cmd_vel_pub_ = self.create_publisher(Twist, "/turtle1/cmd_vel",10) # publisher with a type and a name
        self.prev_x=5.0
        self.prev_y=5.0
        self.has_just_called = False
        self.pose_subscriber = self.create_subscription(
            Pose,
            "/turtle1/pose", 
            self.drive_turtle,
            10
        )
        self.get_logger().info("go_turtle_and_avoid_walls node has just been instanciated")

    def drive_turtle(self, INmsg:Pose):
        # drive turtle
        OUTmsg = Twist()       # create the message
        OUTmsg.linear.x = 1.0
        if abs(5-INmsg.x)>4 or abs(5-INmsg.y)>4:
            OUTmsg.angular.z = pi
            OUTmsg.linear.x = 1.0
            (a,b)=(0,0)
        else:
            if abs(5-INmsg.x)-abs(5-self.prev_x)>0 and abs(5-INmsg.x)>2:
                a=abs(2-abs(5-INmsg.x))
            else:
                a=0
            if abs(5-INmsg.y)-abs(5-self.prev_y)>0 and abs(5-INmsg.y)>2:
                b=abs(2-abs(5-INmsg.y))
            else:
                b=0
            OUTmsg.angular.z = 1.0*(pi/6)*(a+b)
            OUTmsg.linear.x = (1.0+(4-a-b))/2
        # self.get_logger().info("x,y,a,b,s,t: "+str((INmsg.x,INmsg.y,a,b,OUTmsg.angular.z,OUTmsg.linear.x)))
        self.cmd_vel_pub_.publish(OUTmsg)
        if ((INmsg.x<5 and self.prev_x>=5) or (INmsg.x>5 and self.prev_x<=5) or
            (INmsg.y<5 and self.prev_y>=5) or (INmsg.y>5 and self.prev_y<=5)) and not self.has_just_called:
            self.set_color(INmsg.x, INmsg.y)
            self.get_logger().info("Set_color called")
            self.has_just_called = True
        else:
            self.has_just_called = False
        (self.prev_x,self.prev_y) = (INmsg.x, INmsg.y)
        
    def set_color(self,x,y):
        if x<5:
            r=0
        else:
            r=150
        if y<5:
            b=150
        else:
            b=0
        self.call_set_pen_service(r,0,b,3,0)
            


    def call_set_pen_service(self, r, g, b, width, off):
        client = self.create_client(SetPen,"/turtle1/set_pen")
        while not client.wait_for_service():
            self.get_logger().warn("Wainting for server...")
        request = SetPen.Request()
        request.r=r
        request.b=b
        request.g=g
        request.width=width
        request.off=off
        future = client.call_async(request)
        future.add_done_callback(partial(self.callback_set_pen))

    def callback_set_pen(self,future):
        try:
            response = future.result()
        except Exception as e:
            self.get_logger().error("Service call failed: %r" %(e,))


def main(args=None):
    rclpy.init(args=args)   # Mandatory to initiate a ROS2 python program
    node = GoAndAvoidWalls()          # Create a Node
    rclpy.spin(node)        # keep the node alive
    rclpy.shutdown()        # Mandatory to end a ROS2 python program

if __name__=="__main__":
    main()