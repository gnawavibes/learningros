from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='play_with_turtle',
            executable='draw_circle',
            namespace='pen',              # optionnal
            name='writer',                     # optionnal
            remappings=[                                    # optionnal
                 ("/turtle1/cmd_vel","/bolid/cmd_vel"),
            ],
            parameters=[{
                "test_param":"PARAM QUI PASSE!",
            }],
            output="screen",                                # optionnal
        ),
        Node(
            package='play_with_turtle',
            executable='turtle_pose_subscriber',
            namespace='reader',              # optionnal
            remappings=[                                    # optionnal
                ("/turtle1/pose","/bolid/pose"),
            ],
            name='glasses',           # optionnal
            output="screen",                                # optionnal
        ),
        Node(
            package='turtlesim',
            executable='turtlesim_node',
            namespace='bolid',              # optionnal
            name='Vroum_vroum',           # optionnal
            remappings=[                                    # optionnal
                ("/bolid/turtle1/pose","/bolid/pose"),
                ("/bolid/turtle1/cmd_vel","/bolid/cmd_vel"),
            ],
        ),
    ])
