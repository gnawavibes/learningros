#  ROS cheat sheet

<!-- vscode-markdown-toc -->
* 1. [ROS2 commands](#ROS2commands)
	* 1.1. [Node](#Node)
	* 1.2. [Topics](#Topics)
	* 1.3. [Services](#Services)
	* 1.4. [parameters](#parameters)
	* 1.5. [RQT_graph](#RQT_graph)
* 2. [ROS2 WorkSpace and package](#ROS2WorkSpaceandpackage)
	* 2.1. [workspace](#workspace)
	* 2.2. [package](#package)
	* 2.3. [package interfaces](#packageinterfaces)
		* 2.3.1. [ Messages](#Messages)
		* 2.3.2. [ Services](#Services-1)
		* 2.3.3. [ Action](#Action)
	* 2.4. [Launch files](#Launchfiles)
		* 2.4.1. [integrate launch files in a package](#integratelaunchfilesinapackage)
		* 2.4.2. [launch file templae](#launchfiletemplae)
	* 2.5. [parameters config (YAML) files](#parametersconfigYAMLfiles)
* 3. [File/Folder Structure for Packages](#FileFolderStructureforPackages)
	* 3.1. [python package structure](#pythonpackagestructure)
	* 3.2. [C++ package structure](#Cpackagestructure)
	* 3.3. [C++/python package structure](#Cpythonpackagestructure)
* 4. [Python Node](#PythonNode)
	* 4.1. [Create a python node](#Createapythonnode)
	* 4.2. [callback in python](#callbackinpython)
	* 4.3. [create a publisher](#createapublisher)
	* 4.4. [create a suscriber](#createasuscriber)
	* 4.5. [create a service client](#createaserviceclient)
	* 4.6. [adding parameters to a node](#addingparameterstoanode)
* 5. [C++ Node](#CNode)
	* 5.1. [Create a C++ node](#CreateaCnode)
	* 5.2. [callback in C++](#callbackinC)
	* 5.3. [create a publisher](#createapublisher-1)
	* 5.4. [create a suscriber](#createasuscriber-1)
	* 5.5. [create a service client](#createaserviceclient-1)
* 6. [URDF](#URDF)
	* 6.1. [a urdf file template](#aurdffiletemplate)
	* 6.2. [link elements(paramteres)](#linkelementsparamteres)
	* 6.3. [the joints types](#thejointstypes)
	* 6.4. [the joints elements(parameters)](#thejointselementsparameters)
	* 6.5. [xacro tips](#xacrotips)
	* 6.6. [general tips](#generaltips)
* 7. [Rviz-TF2](#Rviz-TF2)
	* 7.1. [robot_state_publisher Node](#robot_state_publisherNode)
		* 7.1.1. [Get TF2 from URDF file](#GetTF2fromURDFfile)
		* 7.1.2. [Get TF2 from Xacro file](#GetTF2fromXacrofile)
	* 7.2. [Rviz](#Rviz)
		* 7.2.1. [open Rviz with ROS2](#openRvizwithROS2)
		* 7.2.2. [visualize TFs in Rviz](#visualizeTFsinRviz)
	* 7.3. [Create a Rviz configuration file](#CreateaRvizconfigurationfile)
	* 7.4. [drive the TF with a simple GUI](#drivetheTFwithasimpleGUI)
	* 7.5. [Make a Launch file of it](#MakeaLaunchfileofit)
* 8. [Gazebo](#Gazebo)
	* 8.1. [Prerequisites](#Prerequisites)
	* 8.2. [Modify the  URDF file](#ModifytheURDFfile)
* 9. [ Nav2](#Nav2)
* 10. [ MoveIt2](#MoveIt2)
* 11. [docker for ROS](#dockerforROS)
	* 11.1. [docker main commands](#dockermaincommands)
* 12. [FAQ](#FAQ)
	* 12.1. [package.xml issue](#package.xmlissue)
	* 12.2. [launch error](#launcherror)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

--------------------------------------------------

##  1. <a name='ROS2commands'></a>ROS2 commands

###  1.1. <a name='Node'></a>Node

- list nodes running: `ros2 node list` 
- get node info: `ros2 node info /<node_name>`
- run a node: `ros2 run <package_name> <executable_name>`

###  1.2. <a name='Topics'></a>Topics

- list topics running: `ros2 topic list` 
- get topic info: `ros2 topic info /<topic_name>`
- listen to a specific topic: `ros2 topic echo /<topic_name>`
- get information about the type of messages exchanged through a topic (type value when asked by topic info command): `ros2 interface show <type/of/message>`
- get topic publishing frequency: `ros2 topic hz /<topic_name>`

###  1.3. <a name='Services'></a>Services

- list services available: `ros2 service list`
- get service type: `ros2 service type /<service_name>`
- get information about the type of service called (type value when asked by service type command): `ros2 interface show <type/of/service>`
- call a service: `ros2 service call <service_name> <service_type> <arguments>` with `<arguments>` written as a stringified dict.

###  1.4. <a name='parameters'></a>parameters
- list parameters available: `ros2 param list`
- get parameter value: `ros2 param get <node_name> <parameter_name>`
- set parameter velue: `ros2 param set <node_name> <parameter_name> <value>`


###  1.5. <a name='RQT_graph'></a>RQT_graph

for a visual diagram of the current ROS architecture running, simply write the folliowing command: `rqt_graph`

--------------------------------------------------

##  2. <a name='ROS2WorkSpaceandpackage'></a>ROS2 WorkSpace and package

###  2.1. <a name='workspace'></a>workspace

1. Create WorkSpace directory `<WS>`
2. create a src dir in it: `<WS>/src`
3. init workspace with `colcon build`
4. Add ./install/setup.bash to .bschrc

###  2.2. <a name='package'></a>package
#### Create package
5. Go to src and create a package: `ros2 pkg create <package_name> --build-type ament_<cmake_or_python> --dependencies <dependencies (at least rclpy or rclcpp)>`

*Note:* If no `--build-type` is provided, `ament_cmake` is the default choice

6. `colcon build` at WS root. (install setuptools v58.2 to fix warning error)

#### import a package

> [!TIP] 
> - install rosdep fist: `sudo apt install python3-bloom python3-rosdep fakeroot debhelper dh-python`
> - then initialize it : `sudo rosdep init`  then 
`rosdep update`

Each time a ROS node is cloned from an external repository, a good practise is to run: `rosdep install --from-paths src --ignore-src -r -y
` in order to find and recompile all the dependencies.

###  2.3. <a name='packageinterfaces'></a>package interfaces

This is a new package that will contains all the custom messages, services and actions of the package we have made. It is a convention probably due to the fact we must use **cmake**  to build those custom interfaces. Thus:

7. Go to `<WS>/src` and create a package: `ros2 pkg create <package_name>_interfaces --build-type ament_cmake`.

*note*: By convention, we keep the `<package_name>` and add `_interfaces` or `_msg` to name that package in reference to the original package named `<package_name>`.

8. Go to the package-interface directory (`cd ~/<WS>/src/<package_name>_interfaces`) Create a folder for each interface (aka messages, services and action): `mkdir msg srv action`

9. Add your custom interfaces:

####  2.3.1. <a name='Messages'></a> Messages

  1. Create a `<MessageName>.msg` file in the `~/<WS>/src/<package_name>_interfaces/msg` directory

##### New instance
  2. Eddit the `<MessageName>.msg` file to create a message from scratch by defining the list of attributes like this:
```
fieldtype1 fieldname1
fieldtype2 fieldname2
fieldtype3 fieldname3
```

[see the complete list of field types](https://docs.ros.org/en/iron/Concepts/Basic/About-Interfaces.html#field-types)

add boundaries to the fieldtype definitions:

```
int32[] unbounded_integer_array
int32[5] five_integers_array
int32[<=5] up_to_five_integers_array

string string_of_unbounded_size
string<=10 up_to_ten_characters_string

string[<=5] up_to_five_unbounded_strings
string<=10[] unbounded_array_of_strings_up_to_ten_characters_each
string<=10[<=5] up_to_five_strings_up_to_ten_characters_each
```

#####  Inheritance

  2. Eddit the `<MessageName>.msg` file and begin by adding the `<Parent/Msg/type> <instance_name>` of the parents instance you want to have.
  3. you can also add other attributes. The final result is something like:

  ```
  <Parent/Msg/type_1> parent1
  <Parent/Msg/type_2> parent2

  fieldtype1 fieldname1
  fieldtype2 fieldname2
  fieldtype3 fieldname3
  ```

####  2.3.2. <a name='Services-1'></a> Services

####  2.3.3. <a name='Action'></a> Action

###  2.4. <a name='Launchfiles'></a>Launch files
####  2.4.1. <a name='integratelaunchfilesinapackage'></a>integrate launch files in a package
1. Create a `launch` folder in the package

##### Python package

2. Add the folliowing lines in the `setup.py` file:
```python
import os
from glob import glob
# Other imports ...

package_name = '<package_name>'

setup(
    # Other parameters ...
    data_files=[
        # ... Other data files
        # Include all launch files.
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*')))
    ]
)
```
##### C++ package

2.Add the folliowing lines in the `CMakeLists.txt` file:
```cmake
# Install launch files.
install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}/
)
```
####  2.4.2. <a name='launchfiletemplae'></a>launch file templae
1. create a launch file in the launch project with the following syntax: `<name_of_launchfile>.launch.py`

2. Create the minimum launchfile:

```python
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([

      """
      Add Node descpritions here...
      Coma separated! (we are in a list)
      """

    ])
```

3. define the nodes to launch inside the LaunchDescription in compliance with the following template:
```python

Node(
    package='<package_name>',
    executable='<executable_name>',
    namespace='<namespace_name>',             # optionnal
    name='<rename_the_node_here>',            # optionnal
    remappings=[                              # optionnal
        ("<topic_name>","<new_topic_name>"),
    ],
    parameters=(                              # optionnal
      {"<parameter1>":<value1>},
      {"<parameter2>":<value2>},
    )
),

``` 
**Namespace:** When launching the two same instance of nodes, the only difference between them is their namespace values. Unique namespaces allow the system to start two nodes without node name or topic name conflicts.

> [!WARNING]
> Using `name` and `namespace` parameters can be source of mistake when linking topics between nodes

> [!TIP] 
> Use **rqt_graph** to help setting up `remapings` when using `name` and `namespace` parameters to link topics


4. Add `<exec_depend>ros2launch</exec_depend>` to the `package.xml` file


###  2.5. <a name='parametersconfigYAMLfiles'></a>parameters config (YAML) files
parameters files are `.yaml` files that are usualy saved in a `config` folder. thus:
- start by creating the config folder: `mkdir config`
- to create a parameter file from a node: `ros2 param dump /<node_name> > config/<parameter_filename>.yaml`
- to load a parameter file to a node: `ros2 param load /<node_name> config/<parameter_filename>.yaml`
- to load when starting a node: `ros2 run <package_name> <executable_name> --ros-args --params-file config/<parameter_filename>.yaml`

##  3. <a name='FileFolderStructureforPackages'></a>File/Folder Structure for Packages

###  3.1. <a name='pythonpackagestructure'></a>python package structure
```
python_struct
├── package.xml
├── python_struct
│   └── __init__.py
├── resource
│   └── python_struct
├── setup.cfg
├── setup.py
├── config
│   └── some_params.yaml
├── launch
│   └── my_application.launch.py
└── test
    ├── test_1.py
    ├── test_2.py
    └── test_3.py
```
###  3.2. <a name='Cpackagestructure'></a>C++ package structure

```
package_name
├── CMakeLists.txt
├── config
│   └── some_param.yaml
├── include
│   └── my_cpp_pkg
├── launch
│   └── my_app.launch.py
├── package.xml
└── src
    └── my_cpp_node.cpp

```

###  3.3. <a name='Cpythonpackagestructure'></a>C++/python package structure

```
package_name
# --> Python stuff
├── package_name
│   ├── __init__.py
│   └── module_to_import.py
├── scripts
│   └── node.py
# C++ stuff
|— include/package_name
|   ├── Class1.hpp
|   └── node1_name_node.hpp
|— src
|   ├── Class1.cpp
|   └── node1_name_node.cpp
├── test
|   ├── Class1Test.cpp
|   ├── Class2Test.cpp
|   └── test_package_name.cpp
├── CMakeLists.txt
├── package.xml
├── config
│   ├── robots
│   │   ├── robot1.yaml
│   │   └── robot2.yaml
│   └── sensors
│       ├── sensor1.yaml
│       └── sensor2.yaml
|— launch
|   ├── node1_name.launch
|   └── node2_name.launch
|— rviz
|   └── package_name.rviz
├── model
|   ├── robot1_repr.urdf
|   └── robot2_repr.xacro
└── worlds
    ├── world1.world
    └── world2.world

```
> ***__NOTE__***:
> In order to achieve a CPP/Python package like this first create a C++ package (`ament_cmake`), then use ament_cmake_python to add python nodes.
see [doc](https://docs.ros.org/en/iron/How-To-Guides/Ament-CMake-Python-Documentation.html), [tutorial](https://roboticsbackend.com/ros2-package-for-both-python-and-cpp-nodes/) and [video](https://www.youtube.com/watch?v=RoRq4XpDEtQ)

For ROS messages, services and action definitions use:

```
package_name_interfaces
├── action
|	  └── MyAction.action
├── msg
|	  └── MyMessage.msg
├── srv
|	  └── MyService.srv
├── CMakeLists.txt
└──  package.xml
```
--------------------------------------------------

##  4. <a name='PythonNode'></a>Python Node

###  4.1. <a name='Createapythonnode'></a>Create a python node

1. Create a python file `<node_filename>.py` in `WS/src/<package_name>/<package_name>`
2. Make the python file executable `chmod +x <node_filename>.py`
3. create a main function like this: `def main(args=None)`
4. In the main function, start a ROS program by using `rclpy.init(args=args)`
5. End the main function by `rclpy.shutdown()` to end the ROS program
6. create a node object by inherit rclpy.node.Node starting constrcutor by `super().__init__("<node_name>")`
7. create the node instance between start and end ROS program
8. add a rclpy.spin(node) to keep the node alive (before ending the program ofc)

**Template** (3-->8)
```python
#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

class <class_node_name>(Node):
    def __init__(self):
        super().__init__("<node_name>")    # node name
        """
        Node construction (attributes) here
        """

def main(args=None):
    rclpy.init(args=args)         # Mandatory to initiate a ROS2 python program
    node = <class_node_name>()    # Create a Node
    rclpy.spin(node)              # keep the node alive
    rclpy.shutdown()              # Mandatory to end a ROS2 python program

if __name__=="__main__":
    main()
```

9. in the `WS/src/<package_name>/setup.py` file add: `"<executable_name> = <package_name>.<package_name>:main"` in the "console_script:{ }" dict of the "entry_points"

> by convention, it is better to use the same name for the <node_filename>, the <node_name> and the <executable_name>

10. Add all the python dependencies (aka librairies imported libraries and other packages linked to the node) by adding a `<depend>'the_dependecy_to_add'</depend>` line in the `package.xml` file

11. Do a `colcon build --symlink-install` at WS root. (--symlink-install used for avoid build each time save python file script)
12. /!\ `source ~/.bashrc` to be able to have the created package` /!\

###  4.2. <a name='callbackinpython'></a>callback in python

1. create a 'timer callback' method in the node class (example: `def <timer_callback_function>(self)`)
2. add a `self.create_timer(<time_period_in_sec>, self.<timer_callback_function>)` attribute in the node constructor

**Template**
```python
#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

class <class_node_name>(Node):
    def __init__(self):
        super().__init__("<node_name>")    # node name
        """
        Node construction (attributes) here
        """
        self.create_timer(<time_period_in_sec>,self.<timer_callback_function>)

    def <timer_callback_function>(self):
        """
        Code to call each <time_period_in_sec> seconds
        """

def main(args=None):
    rclpy.init(args=args)         # Mandatory to initiate a ROS2 python program
    node = <class_node_name>()    # Create a Node
    rclpy.spin(node)              # keep the node alive
    rclpy.shutdown()              # Mandatory to end a ROS2 python program

if __name__=="__main__":
    main()
```

###  4.3. <a name='createapublisher'></a>create a publisher

1. Add a publisher in the node constructor as an attribute: `self.<publisher_name> = self.create_publisher(<type_of_topic>, "<topic_name",<queue_size(int)>)`

  >The <type_of_topic> can be found using commands `ros2 topic list` and `ros2 topic info /<topic_name>`
2. define a methode that will build the message to publish:
    - instanciate a <type_of_topic> class (exemple: `<msg> = <type_of_topic>()`)
    - modify/fill it: `<msg>.<attribute_to_set> = <value>`
    - publish it: `self.<publisher_name>.publish(<msg>)`
3. use a callback to make it spin!

###  4.4. <a name='createasuscriber'></a>create a suscriber

1. Add a subscriber in the node constructor as an attribute: `self.<publisher_name> = self.create_subscription(<type_of_topic>, "<topic_name",<callback_function>,<queue_size(int)>)`

2. define the `<callback_function>` as a methode that way:`def <callback_function>(self, <msg_var>:<type_of_topic>):`; where `<msg_var>` will the topic message recieved

###  4.5. <a name='createaserviceclient'></a>create a service client

1. Add a client in the node constructor as an attribute or in a call service function: `(self.)<client_name> = self.create_client(<type_of_service>, "<service_name")`
2. Wait for server to be ready to recieve the request you want to make:
```
while not client.wait_for_service():
    self.get_logger().warn("Wainting for server...")
```
3. Generate the request by calling the `Request` method of the service you want to use: `request = <type_of_service>.Request()`
4. fill the `request` object with the attributes value you want to pass for the service call
5. generate the `future` expected response from the server to work with it and wait for it: `future = client.call_async(request`
6. **Do not forget** to import `partial` from `functools`: `from functools import partial` 
7. Wait for the service answer: `future.add_done_callback(partial(self.callback_service))` or  `rclpy.spin_until_future_complete(self, self.future)`
8. catch the service answer: `result = future.result()`
9. treat the service result if needed.

finally a standart service call function should look like this:

**Template**
```python
def call_service(self,*args*):
        client = self.create_client(<type_of_service>, "<service_name")
        while not client.wait_for_service():
            self.get_logger().warn("Wainting for server...")
        request = <type_of_service>.Request()
        
        ## request.x = args[x] depending on the args you passed to the function

        future = client.call_async(request)
        future.add_done_callback(partial(self.callback_service))
        ## or  rclpy.spin_until_future_complete(self, self.future)
        result = future.result()

        ## exploit result

    def callback_service(self,future):
        try:
            response = future.result()
        except Exception as e:
            self.get_logger().error("Service call failed: %r" %(e,))
```

###  4.6. <a name='addingparameterstoanode'></a>adding parameters to a node

1. To add a parameter in to a Node, it must be declare in the node constructor after the Node as been instaciater with `super().__init__()` like this:
```python
from rcl_interfaces.msg import ParameterDescriptor
<parameter_descriptor> = ParameterDescriptor(description='<description_of_the_parameter_that_will_be_created>')
self.declare_parameter('<parameter_name>', <default_value_such_as_int_str_or_others>, <parameter_descriptor>)
```

2. Once the package is build, it can be accessed (get) by the ROS commands define in [1.4](#parameters)

3. inside the Node definition, the defined can be accessed (in a callback funciton for example) with the instruction:
```python
my_param = self.get_parameter('<parameter_name>').get_parameter_value().string_value
```

4. inside the Node definition, the defined can be set (in a callback funciton for example) with the instruction:
```python
my_new_param = rclpy.parameter.Parameter(
    '<parameter_name>',
    rclpy.Parameter.Type.<TYPE>,
    <new_value_with_the_correct_type>
)
all_new_parameters = [my_new_param]
self.set_parameters(all_new_parameters)
```
The available `<TYPE>` in `rclpy.Parameter.Type` are:
- 'BOOL'
- 'BOOL_ARRAY'
- 'BYTE_ARRAY'
- 'DOUBLE'
- 'DOUBLE_ARRAY'
- 'INTEGER'
- 'INTEGER_ARRAY'
- 'NOT_SET'
- 'STRING'
- 'STRING_ARRAY'





--------------------------------------------------

##  5. <a name='CNode'></a>C++ Node

###  5.1. <a name='CreateaCnode'></a>Create a C++ node
1. Create a c++ file `<node_filename>.cpp` in `WS/src/<package_name>/src`.
2. Edit the created file and create a main function like this:
```cpp
 int main(int argc, char * argv[])
  {
    // To be filled
    return 0;
  }
```
3. Initialise the ROS program by starting the main with: `rclcpp::init(argc, argv);`
4. end the ROS program by adding `rclcpp::shutdown();` at the end of the main
5. create a node object inheriting `rclcpp::Node`
6. Spin the node inside the ROS program: `rclcpp::spin(std::make_shared<NodeName>());`

  **Template**
  ```cpp
  #include <chrono>
  #include <functional>
  #include <memory>
  #include <string>

  #include "rclcpp/rclcpp.hpp"
  #include "std_msgs/msg/string.hpp"

  using namespace std::chrono_literals;

  /* This example creates a subclass of Node and uses std::bind() to register a
  * member function as a callback from the timer. */

  class NodeName : public rclcpp::Node
  {
    public:
      NodeName()                          // class name
      : Node("<node_name>"),     // instanciated from NodeName class
      {
        //     Node construction (attributes) here

      }

  };

  int main(int argc, char * argv[])
  {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<NodeClass>());
    rclcpp::shutdown();
    return 0;
  }
  ```

3. Add all the C++ dependencies (aka librairies imported libraries and other packages linked to the node) by adding a `<depend>'the_dependecy_to_add'</depend>` line in the `package.xml` file. 
*In the previous example, add `rclcpp` and `std_msgs` has dependancies*

4. In the `CmakeList.txt`, firts add the dependencies under the *find dependencies* comments (and bellow `find_package(ament_cmake REQUIRED)` that has been generated by the created package):

```cmake
# find dependencies
find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
find_package(<other_package> REQUIRED)
```
*note:* the `REQUIRED` option is a cmake option not ROS2

5. Still in the `CmakeList.txt`, then add the executables right after:
```cmake
add_executable(<node_name> src/<node_filename>.cpp)
ament_target_dependencies(<node_name> rclcpp std_msgs <other_package> )
```

6.Still in the `CmakeList.txt`, make the package findable by ROS2:
```Cmake
install(TARGETS
  <node_name> 
  DESTINATION lib/${PROJECT_NAME})
```

11. Do a `colcon build --symlink-install` at WS root. (--symlink-install used for avoid build each time save python file script)
12. /!\ `source ~/.bashrc` to be able to have the created package` /!\

###  5.2. <a name='callbackinC'></a>callback in C++

###  5.3. <a name='createapublisher-1'></a>create a publisher

###  5.4. <a name='createasuscriber-1'></a>create a suscriber

###  5.5. <a name='createaserviceclient-1'></a>create a service client

--------------------------------------------------

##  6. <a name='URDF'></a>URDF

###  6.1. <a name='aurdffiletemplate'></a>a urdf file template

```xml
<?xml version="1.0"?>
<robot name="ROBOTNAME">

<!-- Standart URDF file -->

  <material name="MATERIAL_NAME_1">
    <color rgba="0 0 0.8 1"/>
  </material>

  <material name="MATERIAL_NAME_2">
    <color rgba="1 1 1 1"/>
  </material>

  <link name="LINK_NAME_1">
    <visual>
      <geometry>
        <cylinder length="0.6" radius="0.2"/>
      </geometry>
	    <origin rpy="0 1.57075 0" xyz="0 0 -0.3"/>
      <material name="MATERIAL_NAME_1"/>
    </visual>
  </link>

  <link name="LINK_NAME_2">
    <visual>
      <geometry>
        <box size="0.6 0.1 0.2"/>
      </geometry>
      <origin rpy="0 1.57075 0" xyz="0 0 -0.3"/>
      <material name="MATERIAL_NAME_2"/>
    </visual>
    <inertial>
      <origin xyz="0 0 0.5" rpy="0 0 0"/>
      <mass value="1"/>
      <inertia ixx="100"  ixy="0"  ixz="0" iyy="100" iyz="0" izz="100" />
    </inertial>
  </link>

  <joint name="JOINT_NAME_1" type="TYPE_1">
    <parent link="LINK_NAME_1"/>
    <child link="LINK_NAME_2"/>
    <origin xyz="0 -0.22 0.25"/>
    <axis xyz="0 0 1"/>
    <limit effort="1000.0" lower="0.0" upper="0.548" velocity="0.5"/>
  </joint>
</robot>

<!-- ##################################################  -->
<!--                  Xarco macro Tips                   -->
<!-- ##################################################  -->

<?xml version="1.0"?>
<robot xmlns:xacro="http://www.ros.org/wiki/xacro" name="firefighter"> <!-- Mandatory!-->

<xacro:include filename="$(find package)/other_file.xacro" />
<xacro:include filename="other_file.xacro" ns="namespace"/>


  <xacro:property name="VARIABLE1" value="${namespace.property}" />
  <xacro:property name="VARIABLE2" value="0.4" />
  <xacro:property name="VARIABLE3" value="aBasicString" />

  <xacro:macro name="MACRO_NAME" params="PARAM1 PARAM2">
      <link name="${PARAM1}_leg">
          <visual>
              <geometry>
                  <box size="${VARIABLE1} 0.1 0.2"/>
              </geometry>
              <origin xyz="0 0 -${VARIABLE1/2}" rpy="0 ${pi/2} 0"/>
              <material name="white"/>
          </visual>
          <collision>
              <geometry>
                  <box size="${VARIABLE1} 0.1 0.2"/>
              </geometry>
              <origin xyz="0 0 -${VARIABLE1/2}" rpy="0 ${pi/2} 0"/>
          </collision>
          <xacro:default_inertial mass="10"/>
      </link>

      <joint name="base_to_${PARAM1}_leg" type="fixed">
          <parent link="LINK_NAME_1"/>
          <child link="${PARAM1}_leg"/>
          <origin xyz="0 ${PARAM2*(VARIABLE2+.02)} 0.25" />
      </joint>
  </xacro:macro>
  <xacro:MACRO_NAME PARAM1="right" PARAM2="1" />
  <xacro:MACRO_NAME PARAM1="left" PARAM2="-1" />

  
</robot>

```
###  6.2. <a name='linkelementsparamteres'></a>link elements(paramteres)
* **`<inertial>`**(optional: defaults to a zero mass and zero inertia if not specified)

    The link’s mass, position of its center of mass, and its central inertia properties.

    * `<origin>` (optional: defaults to identity if not specified)
        This pose (translation, rotation) describes the position and orientation of the link’s center of mass frame C relative to the link-frame L.

        **xyz** (optional: defaults to zero vector)
            Represents the position vector from Lo (the link-frame origin) to Co (the link’s center of mass) as x L̂x + y L̂y + z L̂z, where L̂x, L̂y, L̂z are link-frame L's orthogonal unit vectors. 

        **rpy** (optional: defaults to identity if not specified)
            Represents the orientation of C's unit vectors Ĉx, Ĉy, Ĉz relative to link-frame L as a sequence of Euler rotations (r p y) in radians. Note: Ĉx, Ĉy, Ĉz do not need to be aligned with the link’s principal axes of inertia. 

   * `<mass>`

        The mass of the link is represented by the value attribute of this element 

    * `<inertia>`

        This link's moments of inertia ixx, iyy, izz and products of inertia ixy, ixz, iyz about Co (the link’s center of mass) for the unit vectors Ĉx, Ĉy, Ĉz fixed in the center-of-mass frame C. Note: the orientation of Ĉx, Ĉy, Ĉz relative to L̂x, L̂y, L̂z is specified by the rpy values in the <origin> tag. The attributes ixx, ixy, ixz, iyy, iyz, izz for some primitive shapes are here. URDF assumes a negative product of inertia convention (for more info, see these MathWorks docs for working with CAD tools). The simplest way to avoid compatibility issues associated with the negative sign convention for product of inertia is to align Ĉx, Ĉy, Ĉz with principal inertia directions so that all the products of inertia are zero. 
<br>
* **`<visual>`** (optional)

    The visual properties of the link. This element specifies the shape of the object (box, cylinder, etc.) for visualization purposes. Note: multiple instances of <visual> tags can exist for the same link. The union of the geometry they define forms the visual representation of the link.

    **name** (optional)
        Specifies a name for a part of a link's geometry. This is useful to be able to refer to specific bits of the geometry of a link. 

    * `<origin>` (optional: defaults to identity if not specified)
        The reference frame of the visual element with respect to the reference frame of the link.

        **xyz** (optional: defaults to zero vector)
            Represents the x, y, z offset. 

        **rpy** (optional: defaults to identity if not specified)
            Represents the fixed axis roll, pitch and yaw angles in radians. 

    * `<geometry>` (required)

        The shape of the visual object. This can be one of the following:

        * `<box>`

            size attribute contains the three side lengths of the box. The origin of the box is in its center. 

        * `<cylinder>`

            Specify the radius and length. The origin of the cylinder is in its center. cylinder_coordinates.png 

        * `<sphere>`

            Specify the radius. The origin of the sphere is in its center. 

        * `<mesh>`

            A trimesh element specified by a filename, and an optional scale that scales the mesh's axis-aligned-bounding-box. Any geometry format is acceptable but specific application compatibility is dependent on implementation. The recommended format for best texture and color support is Collada .dae files. The mesh file is not transferred between machines referencing the same model. It must be a local file. Prefix the filename with package://<packagename>/<path> to make the path to the mesh file relative to the package <packagename>. 

    * `<material>` (optional)
        The material of the visual element. It is allowed to specify a material element outside of the 'link' object, in the top level 'robot' element. From within a link element you can then reference the material by name.

        name name of the material

        * `<color>` (optional)

            rgba The color of a material specified by set of four numbers representing red/green/blue/alpha, each in the range of [0,1]. 

        * `<texture>` (optional)

            The texture of a material is specified by a filename 
<br>
* **`<collision>`** (optional)

    The collision properties of a link. Note that this can be different from the visual properties of a link, for example, simpler collision models are often used to reduce computation time. Note: multiple instances of <collision> tags can exist for the same link. The union of the geometry they define forms the collision representation of the link.

    name (optional)
        Specifies a name for a part of a link's geometry. This is useful to be able to refer to specific bits of the geometry of a link. 

    * `<origin>` (optional: defaults to identity if not specified)
        The reference frame of the collision element, relative to the reference frame of the link.
        **xyz** (optional: defaults to zero vector)
            Represents the x, y, z offset. 
        **rpy** (optional: defaults to identity if not specified)
            Represents the fixed axis roll, pitch and yaw angles in radians. 

    * `<geometry>`
        See the geometry description in the above visual element. 


###  6.3. <a name='thejointstypes'></a>the joints types
* **revolute** — a hinge joint that rotates along the axis and has a limited range specified by the upper and lower limits.

* **continuous** — a continuous hinge joint that rotates around the axis and has no upper and lower limits.

* **prismatic** — a sliding joint that slides along the axis, and has a limited range specified by the upper and lower limits.

* **fixed** — this is not really a joint because it cannot move. All degrees of freedom are locked. This type of joint does not require the `<axis>`, `<calibration>`, `<dynamics>`, `<limits>` or `<safety_controller>`.

* **floating** — this joint allows motion for all 6 degrees of freedom.

* **planar** — this joint allows motion in a plane perpendicular to the axis. 

###  6.4. <a name='thejointselementsparameters'></a>the joints elements(parameters)
The joint element has following elements:

* `<origin>`(optional: defaults to identity if not specified)
    This is the transform from the parent link to the child link. The joint is located at the origin of the child link, as shown in the figure above.
    **xyz** (optional: defaults to zero vector)
    Represents the x, y, z offset. All positions are specified in metres. 
    **rpy** (optional: defaults to zero vector)
    Represents the rotation around fixed axis: first roll around x, then pitch around y and finally yaw around z. All angles are specified in radians. 
<br>
* `<parent>` (required)
    Parent link name with mandatory attribute:
    **link**(required)
    The name of the link that is the parent of this link in the robot tree structure.
<br>
* `<child>` (required)
    Child link name with mandatory attribute:
    **link**(required)
        The name of the link that is the child link. 
<br>
* `<axis>` (optional: defaults to (1,0,0))
    The joint axis specified in the joint frame. This is the axis of rotation for revolute joints, the axis of translation for prismatic joints, and the surface normal for planar joints. The axis is specified in the joint frame of reference. Fixed and floating joints do not use the axis field.
    **xyz** (required)
    Represents the (x, y, z) components of a vector. The vector should be normalized. 
<br>
* `<calibration>` (optional)
    The reference positions of the joint, used to calibrate the absolute position of the joint.
    **rising** (optional)
    When the joint moves in a positive direction, this reference position will trigger a rising edge. 
    **falling** (optional)
    When the joint moves in a positive direction, this reference position will trigger a falling edge. 
<br>
* `<dynamics>`(optional)
    An element specifying physical properties of the joint. These values are used to specify modeling properties of the joint, particularly useful for simulation.
    **damping** (optional, defaults to 0)
    The physical damping value of the joint (in newton-seconds per metre [N∙s/m] for prismatic joints, in newton-metre-seconds per radian [N∙m∙s/rad] for revolute joints). 
    **friction** (optional, defaults to 0)
    The physical static friction value of the joint (in newtons [N] for prismatic joints, in newton-metres [N∙m] for revolute joints). 
<br>
* `<limit>`(required only for revolute and prismatic joint)
    An element can contain the following attributes:
    **lower** (optional, defaults to 0)
    An attribute specifying the lower joint limit (in radians for revolute joints, in metres for prismatic joints). Omit if joint is continuous. 
    **upper** (optional, defaults to 0)
    An attribute specifying the upper joint limit (in radians for revolute joints, in metres for prismatic joints). Omit if joint is continuous. 
    **effort** (required)
    An attribute for enforcing the maximum joint effort (|applied effort| < |effort|). See safety limits. 
    **velocity** (required)
    An attribute for enforcing the maximum joint velocity (in radians per second [rad/s] for revolute joints, in metres per second [m/s] for prismatic joints). See safety limits. 
<br>
* `<mimic>`(optional) (New with ROS Groovy. See issue)
    This tag is used to specify that the defined joint mimics another existing joint. The value of this joint can be computed as value = multiplier * other_joint_value + offset.
    Expected and optional attributes:
    **joint** (required)
    This specifies the name of the joint to mimic. 
    **multiplier** (optional)
    Specifies the multiplicative factor in the formula above. 
    **offset** (optional)
    Specifies the offset to add in the formula above. Defaults to 0 (radians for revolute joints, meters for prismatic joints) 
<br>
* `<safety_controller>`(optional)
    An element can contain the following attributes:
    **soft_lower_limit** (optional, defaults to 0)
    An attribute specifying the lower joint boundary where the safety controller starts limiting the position of the joint. This limit needs to be larger than the lower joint limit (see above). See See safety limits for more details. 
    **soft_upper_limit** (optional, defaults to 0)
    An attribute specifying the upper joint boundary where the safety controller starts limiting the position of the joint. This limit needs to be smaller than the upper joint limit (see above). See See safety limits for more details. 
    **k_position** (optional, defaults to 0)
    An attribute specifying the relation between position and velocity limits. See See safety limits for more details. 
    **k_velocity** (required)
    An attribute specifying the relation between effort and velocity limits. See See safety limits for more details. 

###  6.5. <a name='xacrotips'></a>xacro tips
* can include files:
  ``` xml
  <xacro:include filename="$(find package)/other_file.xacro" />
  ```
  To avoid name clashes between properties and macros of various included files, you can specify a namespace for the included file - providing the attribute ns: 
  ```xml
  <xacro:include filename="other_file.xacro" ns="namespace"/>
  ```
  And access to namespaced macros and properties is achieved by prepending the namespace, separated by a dot:  `${namespace.property}`

* use macros as template (see urdf file example)
* define variables that wille be reused in a `property` tag 
  ```xml
  <xacro:property name="VARIABLE_NAME" value="VARIABLE_NAME" />
  ```
* or block of of code as property:
  ```xml
  <xacro:property name="front_left_origin">
    <origin xyz="0.3 0 0" rpy="0 0 0" />
  </xacro:property>

  <pr2_wheel name="front_left_wheel">
    <xacro:insert_block name="front_left_origin" />
  </pr2_wheel>
  ```
* use mathematical expressions in values
* use conditionnal blocks:
  ```xml
  <xacro:if value="<expression>">
    <... some xml code here ...>
  </xacro:if>
  <xacro:unless value="<expression>">
    <... some xml code here ...>
  </xacro:unless>
  ```
  or value test:
  ```xml
  <xacro:property name="var" value="useit"/>
  <xacro:if value="${var == 'useit'}"/>
  <xacro:if value="${var.startswith('use') and var.endswith('it')}"/>

  <xacro:property name="allowed" value="${[1,2,3]}"/>
  <xacro:if value="${1 in allowed}"/>
  ```
### Using the meshes for link geometry

1. Add the `meshes` folder in the package folder: `mkdir meshes`

2. modify the `CMakeList.txt` file by adding **meshes** for the folder in:
``` cmake
install(
  DIRECTORY src launch rviz meshes
  DESTINATION share/${PROJECT_NAME}
)
```

3. The mesh (stl or dae file) can now be passed as a geometry:
```xml
<link name="<link_name>">
    <visual>
        <origin rpy="0 0 0" xyz="0.0 0.0 0.0"/>
        <mass value="0.8"/>
        <geometry>
            <mesh filename="package://<package_name>/meshes/<mesh_filename>.<stl_or_dae>"/>
        </geometry>
        <material name="green"/>
    </visual>
</link>
```
> [!WARNING]
> becarefull or your mesh size. It can be resized by adding the **scale** parameter int the **mesh** tags like this: `scale="0.001 0.001 0.001"`

### quick Visualisation in Rviz2
Use the following command thant to the **urdf_tutorial** package:
`ros2 launch urdf_tutorial display.launch.py model:=</absolute/path/to/MyFile>.urdf`

###  6.6. <a name='generaltips'></a>general tips

It is possible to [autogenerate a urdf file from Denavit Hartenberg cordinates](https://adohaha.github.io/DH2URDF/)

--------------------------------------------------

##  7. <a name='Rviz-TF2'></a>Rviz-TF2
###  7.1. <a name='robot_state_publisherNode'></a>robot_state_publisher Node
####  7.1.1. <a name='GetTF2fromURDFfile'></a>Get TF2 from URDF file

`ros2 run robot_state_publisher robot_state_publisher <path/to/urdf/file>`

####  7.1.2. <a name='GetTF2fromXacrofile'></a>Get TF2 from Xacro file

`ros2 run robot_state_publisher robot_state_publisher --ros-args -p robot_description:="$( xacro <~/path/to/file.urdf.xacro> )"`

###  7.2. <a name='Rviz'></a>Rviz
####  7.2.1. <a name='openRvizwithROS2'></a>open Rviz with ROS2

simple command `rviz2`

####  7.2.2. <a name='visualizeTFsinRviz'></a>visualize TFs in Rviz

1. set `fixed frame` to `world`
2. click `Add`-->`TF`
3. In the TF section of the tree click on `show names` to visualize the TF names

###  7.3. <a name='CreateaRvizconfigurationfile'></a>Create a Rviz configuration file

At any moment, the state of Rviz can be saved in a Rviz configuration file: `<name_config>.rviz`; directly from the graphical interface. The good practices want to have all those config files in a `rviz` folder inside the package.


###  7.4. <a name='drivetheTFwithasimpleGUI'></a>drive the TF with a simple GUI

once the **robot_state_publisher** node is running just run the **joint_state_publisher_gui** node:
`ros2 run joint_state_publisher_gui joint_state_publisher_gui `

###  7.5. <a name='MakeaLaunchfileofit'></a>Make a Launch file of it
**Template**
```python
import os
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition, UnlessCondition
from launch.substitutions import Command, LaunchConfiguration
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():

  # Set the path to this package.
  pkg_share = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)) # my version
  # FindPackageShare(package='<package_name>').find('<package_name>')  #tuto version

  # Set the path to the RViz configuration settings
  default_rviz_config_path = os.path.join(pkg_share, 'rviz/<rviz_config_name>.rviz')

  # Set the path to the URDF file
  default_urdf_model_path = os.path.join(pkg_share, 'model/<urdf_name>.urdf')

  ########### YOU DO NOT NEED TO CHANGE ANYTHING BELOW THIS LINE ##############  
  # Launch configuration variables specific to simulation
  gui = LaunchConfiguration('gui')
  urdf_model = LaunchConfiguration('urdf_model')
  rviz_config_file = LaunchConfiguration('rviz_config_file')
  use_robot_state_pub = LaunchConfiguration('use_robot_state_pub')
  use_rviz = LaunchConfiguration('use_rviz')
  use_sim_time = LaunchConfiguration('use_sim_time')

  # Declare the launch arguments  
  declare_urdf_model_path_cmd = DeclareLaunchArgument(
    name='urdf_model', 
    default_value=default_urdf_model_path, 
    description='Absolute path to robot urdf file')
    
  declare_rviz_config_file_cmd = DeclareLaunchArgument(
    name='rviz_config_file',
    default_value=default_rviz_config_path,
    description='Full path to the RVIZ config file to use')
    
  declare_use_joint_state_publisher_cmd = DeclareLaunchArgument(
    name='gui',
    default_value='True',
    description='Flag to enable joint_state_publisher_gui')
  
  declare_use_robot_state_pub_cmd = DeclareLaunchArgument(
    name='use_robot_state_pub',
    default_value='True',
    description='Whether to start the robot state publisher')

  declare_use_rviz_cmd = DeclareLaunchArgument(
    name='use_rviz',
    default_value='True',
    description='Whether to start RVIZ')
    
  declare_use_sim_time_cmd = DeclareLaunchArgument(
    name='use_sim_time',
    default_value='True',
    description='Use simulation (Gazebo) clock if true')
   
  # Specify the actions

  # Publish the joint state values for the non-fixed joints in the URDF file.
  start_joint_state_publisher_cmd = Node(
    condition=UnlessCondition(gui),
    package='joint_state_publisher',
    executable='joint_state_publisher',
    name='joint_state_publisher')

  # A GUI to manipulate the joint state values
  start_joint_state_publisher_gui_node = Node(
    condition=IfCondition(gui),
    package='joint_state_publisher_gui',
    executable='joint_state_publisher_gui',
    name='joint_state_publisher_gui')

  # Subscribe to the joint states of the robot, and publish the 3D pose of each link.
  start_robot_state_publisher_cmd = Node(
    condition=IfCondition(use_robot_state_pub),
    package='robot_state_publisher',
    executable='robot_state_publisher',
    parameters=[{'use_sim_time': use_sim_time, 
    'robot_description': Command(['xacro ', urdf_model])}],
    arguments=[default_urdf_model_path])

  # Launch RViz
  start_rviz_cmd = Node(
    condition=IfCondition(use_rviz),
    package='rviz2',
    executable='rviz2',
    name='rviz2',
    output='screen',
    arguments=['-d', rviz_config_file])
  
  # Create the launch description and populate
  ld = LaunchDescription()

  # Declare the launch options
  ld.add_action(declare_urdf_model_path_cmd)
  ld.add_action(declare_rviz_config_file_cmd)
  ld.add_action(declare_use_joint_state_publisher_cmd)
  ld.add_action(declare_use_robot_state_pub_cmd)  
  ld.add_action(declare_use_rviz_cmd) 
  ld.add_action(declare_use_sim_time_cmd)

  # Add any actions
  ld.add_action(start_joint_state_publisher_cmd)
  ld.add_action(start_joint_state_publisher_gui_node)
  ld.add_action(start_robot_state_publisher_cmd)
  ld.add_action(start_rviz_cmd)

  return ld


```

--------------------------------------------------

##  8. <a name='Gazebo'></a>Gazebo
###  8.1. <a name='Prerequisites'></a>Prerequisites
A URDF file running properly in **Rviz2** thanks to the `robot_state_publisher` node (aka drivable by `joint_state_publisher_gui`). 
That means the URDF file must have an  `<inertia>` element within each `<link>` element that must be properly specified and configured.

[Autres astuces](https://github.com/osrf/subt/wiki/Model-Migration-Guide)

### Generate SDF from URDF file
`ign sdf -p <URDF_filename>.urdf > <SDF_filename>.sdf `

### working with SDF files
Check the [Sdfformat website](http://sdformat.org/) for more details

#### the <include> tag
1. Set your **GZ_SIM_RESOURCE_PATH** environement variable by adding the paths where you will put your `.sdf` files to be included (`<uri>` tags does not support relative paths). Multiple paths can be added separeted by a `:`.
2. include the `.sdf` file to load like this:
```
<include>
      <uri>models/MyRobot</uri>
      <pose>1 1 1 0 0 0</pose>    
    </include>
```


--------------------------------------------------

##  9. <a name='Nav2'></a> Nav2


--------------------------------------------------

##  10. <a name='MoveIt2'></a> MoveIt2


--------------------------------------------------
##  11. <a name='dockerforROS'></a>docker for ROS

###  11.1. <a name='dockermaincommands'></a>docker main commands


##  12. <a name='FAQ'></a>FAQ

###  12.1. <a name='package.xmlissue'></a>package.xml issue
```
[2.956s] WARNING:colcon.colcon_ros.prefix_path.ament:The path '/home/ros2user/learningROS/ROS_WS1/install/play_with_turtle_interfaces' in the environment variable AMENT_PREFIX_PATH doesn't exist
[2.956s] WARNING:colcon.colcon_ros.prefix_path.ament:The path '/home/ros2user/learningROS/ROS_WS1/install/play_with_turtle' in the environment variable AMENT_PREFIX_PATH doesn't exist
[2.956s] WARNING:colcon.colcon_ros.prefix_path.ament:The path '/home/ros2user/learningROS/ROS_WS1/install/gazebo_tuto' in the environment variable AMENT_PREFIX_PATH doesn't exist
[2.956s] WARNING:colcon.colcon_ros.prefix_path.ament:The path '/home/ros2user/learningROS/ROS_WS1/install/Robot1_controller' in the environment variable AMENT_PREFIX_PATH doesn't exist
[2.968s] WARNING:colcon.colcon_ros.prefix_path.catkin:The path '/home/ros2user/learningROS/ROS_WS1/install/play_with_turtle_interfaces' in the environment variable CMAKE_PREFIX_PATH doesn't exist
[2.968s] WARNING:colcon.colcon_ros.prefix_path.catkin:The path '/home/ros2user/learningROS/ROS_WS1/install/gazebo_tuto' in the environment variable CMAKE_PREFIX_PATH doesn't exist

```
`<buildtool_depend>ament_python</buildtool_depend>` or `<buildtool_depend>ament_cmake</buildtool_depend>`are probably missing in a `package.xml file

###  12.2. <a name='launcherror'></a>launch error
```
[ERROR] [launch]: Caught exception in launch (see debug for traceback): "package 'play_with_turle' not found, searching: ['/home/ros2user/learningROS/ROS_WS1/install/play_with_turtle_interfaces', '/home/ros2user/learningROS/ROS_WS1/install/play_with_turtle',
...
]
```
`play_with_turle` package does not exist (here name with an spelling issue in the launch file)